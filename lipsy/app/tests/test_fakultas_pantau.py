from django.test import TestCase
from django.urls import reverse
from django.test import Client
from django.test.client import RequestFactory
from app.models import DocumentBundle, AcceptanceLetter, CalonYangDiusulkan, Application, Status, Level, Faculty, Regulation, StatusChangeRecord, Admin, Record, Regulation, Role, ApplicationHistory, RegulationQuestion
from app.controllers import fakultas_memantau_aplikasi_cyd as fmac
from django.core.exceptions import ObjectDoesNotExist
from mock import patch
from mock import MagicMock
import mock
import unittest
from django.http import HttpResponse
from django.core.files import File
# from app.views import fakultas_views as fv


class HomeFakultasTest(TestCase):

    def create_objects(self):
        role = Role.objects.create(name="DGB")
        admin = Admin.objects.create(nip=1000067891234567, role=role)
        reguler = Regulation.objects.create(name='Reguler')

        teknik = Faculty.objects.create(name = 'Teknik')
        self.teknik = teknik
        diproses_sdm = Status.objects.create(name = 'Diproses SDM')
        diproses_dgb = Status.objects.create(name = 'Diproses DGB')
        dikembalikan = Status.objects.create(name = 'Dikembalikan SDM')
        dikembalikan_dgb = Status.objects.create(name = 'Dikembalikan DGB')
        diterima = Status.objects.create(name = 'Diterima')
        dibatalkan = Status.objects.create(name = 'Dibatalkan')
        lektor = Level.objects.create(name='Lektor')
        lektor_kepala = Level.objects.create(name='Lektor Kepala')
        sumarni = CalonYangDiusulkan.objects.create(nip = '111111', name = 'Sumarni', birthplace = 'Lamongan', current_level = lektor, email = 'sumarni@abc.com', faculty = teknik, gender = 'P')
        sukarwo = CalonYangDiusulkan.objects.create(nip = '111112', name = 'Sukarwo', birthplace = 'Kediri', current_level = lektor, email = 'sukarwo@abc.com', faculty = teknik, gender = 'L')
        ahmad = CalonYangDiusulkan.objects.create(nip = '111113', name = 'Ahmad', birthplace = 'Malang', current_level = lektor, email = 'ahmad@abc.com', faculty = teknik, gender = 'L')
        susan = CalonYangDiusulkan.objects.create(nip = '111114', name = 'Susan', birthplace = 'Depok', current_level = lektor, email = 'susan@abc.com', faculty = teknik, gender = 'P')
        
        Application.objects.create(cyd = sumarni, status = diproses_sdm, regulation = reguler, destination_level = lektor_kepala)
        
        self.waktu_diproses = StatusChangeRecord.objects.create(cyd = sukarwo, status = diproses_sdm, description = 'Diproses SDM', created_at = '2017-03-21 07:43:02').created_at
        self.waktu_dikembalikan_sdm = StatusChangeRecord.objects.create(cyd = sukarwo, status = dikembalikan, description = 'Acceptance letter bermasalah', created_at = '2017-03-22 07:43:02').created_at
        revisi = StatusChangeRecord.objects.create(cyd = sukarwo, status = dikembalikan_dgb, description = 'Catatan: ', created_at = '2017-03-23 07:43:02')
        self.waktu_dikembalikan_dgb = revisi.created_at
        app_sukarwo = Application.objects.create(cyd = sukarwo, status = dikembalikan, regulation = reguler, destination_level = lektor_kepala)

        StatusChangeRecord.objects.create(cyd = ahmad, status = diproses_dgb, description = 'Diproses DGB')
        self.waktu_terima = StatusChangeRecord.objects.create(cyd = ahmad, status = diterima, description = 'Pengajuan diterima').created_at
        app_ahmad = Application.objects.create(cyd = ahmad, status = diterima, regulation = reguler, destination_level = lektor_kepala)
        
        StatusChangeRecord.objects.create(cyd = susan, status = diproses_sdm, description = 'Diproses SDM')
        self.waktu_tolak = StatusChangeRecord.objects.create(cyd = susan, status = dibatalkan, description = 'Pengajuan dibatalkan atas permintaan CYD').created_at
        Application.objects.create(cyd = susan, status = dibatalkan, regulation = reguler, destination_level = lektor_kepala) 
 
        app_hist_sukarwo = ApplicationHistory.objects.create(cyd=sukarwo, description="Perlu revisi", revision_number=1, change_record=revisi)
        
        regulationQuestion = RegulationQuestion.objects.create(creator=admin, regulation=reguler, question="Pernah mengikuti kegiatan X")

        record = Record.objects.create(application_history=app_hist_sukarwo, regulation_question=regulationQuestion,
                                        record="Sertifikat belum dilampirkan")

        self.ahmad = ahmad
        self.sukarwo = sukarwo

        file_mock = mock.MagicMock(spec=File, name='FileMock')
        file_mock.name = 'test1.jpg'

        self.file = file_mock

        self.berkas = DocumentBundle.objects.create(application = app_ahmad, document = 'berkas/2017/03/22/Artikel.zip')
        self.acc = AcceptanceLetter.objects.create(application = app_ahmad, document = 'acc_letter/2017/03/22/bajaj2015.pdf')

    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()
        self.create_objects()

    def fakultas_pantau_aktif_view(self, myrender):
        with patch('app.controllers.fakultas_memantau_aplikasi_cyd.cyd_dikembalikan_list', return_value=1), patch('app.controllers.fakultas_memantau_aplikasi_cyd.cyd_diproses_list', return_value=2):
            url = reverse("fakultas_pantau_aktif")
            resp = self.client.get(url)
            myrender.assert_called()
            args, kwargs = myrender.call_args
            self.assertEqual(resp.status_code, 200)
            self.assertEqual(args[1], 'app/fakultas/fakultas_pantau_aktif.html')
            self.assertEqual(args[2]['cyd_dikembalikan'], 1)
            self.assertEqual(args[2]['cyd_diproses'], 2)
            self.assertEqual(args[2]['user']['role'], 'Fakultas')

    def fakultas_pantau_diterima_view(self, myrender):
        with patch('app.controllers.fakultas_memantau_aplikasi_cyd.cyd_diterima_list', return_value=3):
            url = reverse("fakultas_pantau_diterima")
            resp = self.client.get(url)
            myrender.assert_called()
            args, kwargs = myrender.call_args
            self.assertEqual(resp.status_code, 200)
            self.assertEqual(args[1], 'app/fakultas/fakultas_pantau_diterima.html')
            self.assertEqual(args[2]['cyd_diterima'], 3)
            self.assertEqual(args[2]['user']['role'], 'Fakultas')

    def fakultas_pantau_dibatalkan_view(self, myrender):
        with patch('app.controllers.fakultas_memantau_aplikasi_cyd.cyd_dibatalkan_list', return_value=4):
            url = reverse("fakultas_pantau_dibatalkan")
            resp = self.client.get(url)
            myrender.assert_called()
            args, kwargs = myrender.call_args
            self.assertEqual(resp.status_code, 200)
            self.assertEqual(args[1], 'app/fakultas/fakultas_pantau_dibatalkan.html')
            self.assertEqual(args[2]['cyd_dibatalkan'], 4)
            self.assertEqual(args[2]['user']['role'], 'Fakultas')
  
    def download_view(self):
        resp_berkas_expect = HttpResponse('resp1')
        resp_acc_expect = HttpResponse('resp2')
        with patch('app.controllers.fakultas_memantau_aplikasi_cyd.download_berkas', return_value = resp_berkas_expect), patch('app.controllers.fakultas_memantau_aplikasi_cyd.download_acc', return_value = resp_acc_expect):
            resp_berkas = self.client.get(reverse('download_berkas', kwargs={'cyd_id':self.ahmad.id}))
            resp_acc = self.client.get(reverse('download_acc', kwargs={'cyd_id':self.ahmad.id}))
            self.assertEqual(resp_berkas.content, resp_berkas_expect.content)
            self.assertEqual(resp_acc.content, resp_acc_expect.content)

    def test_view(self):
        fake_render = MagicMock()
        fake_render.return_value = HttpResponse('')
        with patch('django.shortcuts.render', fake_render) as myrender, patch('app.controllers.fakultas_memantau_aplikasi_cyd.get_cyd', return_value=0):
            self.fakultas_pantau_aktif_view(myrender)
            self.fakultas_pantau_diterima_view(myrender)
            self.fakultas_pantau_dibatalkan_view(myrender)
        self.download_view()

    def test_cyd_dikembalikan(self):
        cyd = fmac.get_cyd(self.teknik.id, 'Dikembalikan')
        for c in cyd:
            self.assertEqual(c.faculty.name, 'Teknik')
            app = Application.objects.get(cyd = c.id)
            self.assertEqual(app.status.name.split()[0], 'Dikembalikan')
  
    def test_cyd_diproses(self):
        cyd = fmac.get_cyd(self.teknik.id, 'Diproses')
        for c in cyd:
            self.assertEqual(c.faculty.name, 'Teknik')
            app = Application.objects.get(cyd = c.id)
            self.assertEqual(app.status.name.split()[0], 'Diproses')

    def test_cyd_dibatalkan(self):
        cyd = fmac.get_cyd(self.teknik.id, 'Dibatalkan')
        for c in cyd:
            self.assertEqual(c.faculty.name, 'Teknik')
            app = Application.objects.get(cyd = c.id)
            self.assertEqual(app.status.name.split()[0], 'Dibatalkan')

    def test_cyd_diterima(self):
        cyd = fmac.get_cyd(self.teknik.id, 'Diterima')
        for c in cyd:
            self.assertEqual(c.faculty.name, 'Teknik')
            app = Application.objects.get(cyd = c.id)
            self.assertEqual(app.status.name.split()[0], 'Diterima')

    def test_tabel_dibatalkan(self):
        dibatalkan = fmac.get_cyd(self.teknik.id, 'Dibatalkan')
        dibatalkan_list = fmac.cyd_dibatalkan_list(dibatalkan)
        for cyd in dibatalkan_list:
            self.assertEqual(cyd['name'], 'Susan')
            self.assertEqual(cyd['nip'], '111114')
            self.assertEqual(cyd['level'], 'Lektor')
            self.assertEqual(cyd['dest_level'], 'Lektor Kepala')
            self.assertEqual(cyd['time'], self.waktu_tolak.date())

    def test_tabel_diterima(self):
        diterima = fmac.get_cyd(self.teknik.id, 'Diterima')
        diterima_list = fmac.cyd_diterima_list(diterima)
        for cyd in diterima_list:
            self.assertEqual(cyd['name'], 'Ahmad')
            self.assertEqual(cyd['nip'], '111113')
            self.assertEqual(cyd['level'], 'Lektor')
            self.assertEqual(cyd['dest_level'], 'Lektor Kepala')
            self.assertEqual(cyd['time'], self.waktu_terima.date())

    def test_tabel_diproses(self):
        diproses = fmac.get_cyd(self.teknik.id, 'Diproses')
        diproses_list = fmac.cyd_diproses_list(diproses)
        for cyd in diproses_list:
            self.assertEqual(cyd['name'], 'Sumarni')
            self.assertEqual(cyd['nip'], '111111')
            self.assertEqual(cyd['level'], 'Lektor')
            self.assertEqual(cyd['dest_level'], 'Lektor Kepala')
            self.assertEqual(cyd['status'], 'Diproses SDM')

    def logtest(self, cyd):
        logs = cyd['log']
        log1 = logs[0]
        self.assertEqual(log1['title'], '1. Diproses SDM pada '+'2017-03-21')
        log2 = logs[1]
        self.assertEqual(log2['title'], '2. Dikembalikan SDM pada '+'2017-03-22')
        self.assertEqual(log2['detail'], 'Acceptance letter bermasalah')
        log3 = logs[2]
        self.assertEqual(log3['title'], '3. Dikembalikan DGB pada '+'2017-03-23')
        self.assertEqual(log3['detail'], 'Catatan: ')
        notes = log3['notes']
        for n in notes:
            self.assertEqual(n.regulation_question.question, 'Pernah mengikuti kegiatan X')
            self.assertEqual(n.record, 'Sertifikat belum dilampirkan')

    def test_tabel_dikembalikan(self):
        dikembalikan = fmac.get_cyd(self.teknik.id, 'Dikembalikan')
        dikembalikan_list = fmac.cyd_dikembalikan_list(dikembalikan)
        for cyd in dikembalikan_list:
            self.assertEqual(cyd['name'], 'Sukarwo')
            self.assertEqual(cyd['nip'], '111112')
            self.assertEqual(cyd['level'], 'Lektor')
            self.assertEqual(cyd['dest_level'], 'Lektor Kepala')
            self.logtest(cyd = cyd)

    def test_exception(self):
        with patch('app.models.Status.objects.get', side_effect = ObjectDoesNotExist):
            with self.assertRaises(fmac.StatusNotFound):
                fmac.get_status([self.ahmad])
            self.assertEqual(fmac.cyd_diproses_list([self.ahmad]), [])
        with patch('app.models.Level.objects.get', side_effect = ObjectDoesNotExist):
            with self.assertRaises(fmac.JabatanNotFound):
                fmac.get_jabatan_dituju([self.ahmad])
            self.assertEqual(fmac.cyd_dikembalikan_list([self.ahmad]), [])
        with patch('app.models.StatusChangeRecord.objects.get', side_effect = ObjectDoesNotExist):
            with self.assertRaises(fmac.WaktuNotFound):
                fmac.get_waktu([self.ahmad], 'Diterima')
            self.assertEqual(fmac.cyd_diterima_list([self.ahmad]), [])
        with patch('app.models.ApplicationHistory.objects.get', side_effect = ObjectDoesNotExist):
            with self.assertRaises(fmac.LogNotFound):
                fmac.get_log([self.sukarwo])
            self.assertEqual(fmac.cyd_dibatalkan_list([self.ahmad]), [])

    def download_success(self):
        resp_berkas = fmac.download_berkas(self.ahmad.id)
        resp_acc = fmac.download_acc(self.ahmad.id)
        berkas = open(self.berkas.document.path, 'rb').read()
        acc = open(self.acc.document.path, 'rb').read()
        self.assertEqual(resp_berkas.content, berkas)
        self.assertEqual(resp_acc.content, acc)

    def download_false_path(self):
        fake_doc = MagicMock()
        fake_doc.document.path = 'randomstring-askdasdasdkk'
        with patch('app.models.DocumentBundle.objects.get', fake_doc), patch('app.models.AcceptanceLetter.objects.get', fake_doc), patch('os.path.exists', return_value=False):
            resp_berkas = fmac.download_berkas(self.ahmad.id)
            resp_acc = fmac.download_acc(self.ahmad.id)
            self.assertEqual(resp_berkas.content, b'<html><h3>Berkas tidak ditemukan</h3></html>')
            self.assertEqual(resp_acc.content, b'<html><h3>Acceptance letter belum tersedia</h3></html>')

    def download_document_not_exist(self):
        with patch('app.models.DocumentBundle.objects.get', side_effect = ObjectDoesNotExist),patch('app.models.AcceptanceLetter.objects.get', side_effect = ObjectDoesNotExist):
            resp_berkas = fmac.download_berkas(self.ahmad.id)
            resp_acc = fmac.download_acc(self.ahmad.id)
            self.assertEqual(resp_berkas.content, b'<html><h3>Berkas tidak ditemukan</h3></html>')
            self.assertEqual(resp_acc.content, b'<html><h3>Acceptance letter belum tersedia</h3></html>')
    
    def test_download(self):
        self.download_success()
        self.download_false_path()
        self.download_document_not_exist()
