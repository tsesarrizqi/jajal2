from django.test import TestCase
from app.models import Role, Admin, Level, Faculty, Status, Regulation, RegulationQuestion, CalonYangDiusulkan, \
    Application, EducationHistory, ApplicationHistory, Record, StatusChangeRecord


class ModelTest(TestCase):
    def test_models(self):
        role = Role.objects.create(name="Fakultas")
        admin = Admin.objects.create(nip=123456789123456789, role=role)
        level = Level.objects.create(name="Lektor kepala")
        faculty = Faculty.objects.create(name="Fakultas Ilmu Komputer")
        status = Status.objects.create(name="Disetujui")
        regulation = Regulation.objects.create(name="Aturan 001")
        a = Admin.objects.get(nip=123456789123456789)
        regulationQuestion = RegulationQuestion.objects.create(creator=a, regulation=regulation,
                                                               question="apakah ijazah asli?")
        cyd = CalonYangDiusulkan.objects.create(nip=123456789123456789, faculty=faculty, current_level=level,
                                                name="Sutopo")
        application = Application.objects.create(status=status, regulation=regulation, destination_level=level,
                                                 cyd=cyd)
        educationHistory = EducationHistory.objects.create(cyd=cyd, order=1, name="SD Harapan Bangsa")
        appHistory = ApplicationHistory.objects.create(cyd=cyd, revision_number=1,
                                                       description="Tidak mendapat persetujuan rektor")
        record = Record.objects.create(application_history=appHistory, regulation_question=regulationQuestion,
                                       record="Ijazah tidak ada legalisir")
        statusChangeRecord = StatusChangeRecord.objects.create(cyd=cyd, status=status,
                                                               description="Ijazah tidak ada legalisir")

        self.assertEqual(str(role), (str(role.id) + " " + role.name))
        self.assertEquals(str(admin), str(admin.nip))
        self.assertEquals(str(level), str(level.id) + " " + level.name)
        self.assertEquals(str(faculty), str(faculty.id) + " " + faculty.name)
        self.assertEquals(str(status), str(status.id) + " " + status.name)
        self.assertEquals(str(regulation), str(regulation.id) + " " + regulation.name)
        self.assertEquals(str(regulationQuestion), str(regulationQuestion.id) + " " + regulationQuestion.question)
        self.assertEquals(str(cyd), str(cyd.id) + " " + cyd.name)
        self.assertEquals(str(application), str(application.id) + " " + str(application.cyd.nip))
        self.assertEquals(str(educationHistory), str(educationHistory.id) + " " + educationHistory.name)
        self.assertEquals(str(appHistory), str(appHistory.id) + " " + appHistory.description)
        self.assertEquals(str(record), str(record.id) + " " + record.record)
        self.assertEquals(str(statusChangeRecord), str(statusChangeRecord.id) + " " + statusChangeRecord.description)
