from django.db.models import Q
from app.models import CalonYangDiusulkan, Application, Faculty, Status, Level, StatusChangeRecord, ApplicationHistory, Record, DocumentBundle, AcceptanceLetter
from django.core.exceptions import ObjectDoesNotExist
import os
from django.http import HttpResponse

class StatusNotFound(Exception):
    pass
class JabatanNotFound(Exception):
    pass
class WaktuNotFound(Exception):
    pass
class LogNotFound(Exception):
    pass
class DocumentNotFound(Exception):
    pass

def download_berkas(cyd_id):
    try:
        app_id = Application.objects.get(cyd = cyd_id).id
        file_path = DocumentBundle.objects.get(application = app_id).document.path
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                response = HttpResponse(fh.read(), content_type="application/force-download")
                response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
                return response
        else:
            return HttpResponse('<html><h3>Berkas tidak ditemukan</h3></html>')
    except ObjectDoesNotExist:
        return HttpResponse('<html><h3>Berkas tidak ditemukan</h3></html>')

def download_acc(cyd_id):
    try:
        app_id = Application.objects.get(cyd = cyd_id).id
        file_path = AcceptanceLetter.objects.get(application = app_id).document.path
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                response = HttpResponse(fh.read(), content_type="application/force-download")
                response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
                return response
        else:
            return HttpResponse('<html><h3>Acceptance letter belum tersedia</h3></html>')
    except ObjectDoesNotExist:
        return HttpResponse('<html><h3>Acceptance letter belum tersedia</h3></html>')

def get_cyd(fakultas, status):
    status_arr = Status.objects.filter(name__contains = status).values_list('id', flat=True)
    nips = Application.objects.filter(status__in = status_arr).values_list('cyd', flat=True)
    cyd = CalonYangDiusulkan.objects.filter(faculty = fakultas, id__in = nips)
    return cyd

def get_status(cyds):
    try:
        status = []
        for cyd in cyds:
            status_id = Application.objects.get(cyd = cyd.id).status.id
            status_name = Status.objects.get(id = status_id).name
            status += [status_name]
        return status
    except ObjectDoesNotExist:
        raise StatusNotFound()

def get_jabatan_dituju(cyds):
    try:
        level = []
        for cyd in cyds:
            level_id = Application.objects.get(cyd = cyd.id).destination_level.id
            level_name = Level.objects.get(id = level_id).name
            level += [level_name]
        return level
    except ObjectDoesNotExist:
        raise JabatanNotFound()

def get_waktu(cyds, status):
    try:
        times = []
        for cyd in cyds:
            status_id = Status.objects.get(name__contains = status).id
            time = StatusChangeRecord.objects.get(cyd = cyd.id, status = status_id).created_at
            times += [time]
        return times
    except ObjectDoesNotExist:
        raise WaktuNotFound()

def get_log(cyds):
    try:
        cyds_log = []
        for cyd in cyds:
            changes = StatusChangeRecord.objects.filter(cyd = cyd.id).order_by('created_at')
            log = []
            count = 1
            for change in changes:
                log_instance = {}
                if change.status.name == 'Dikembalikan SDM' or change.status.name == 'Dibatalkan':
                    log_instance['title'] = str(count) + ". " + str(change.status.name) + " pada " + str(change.created_at.date())
                    log_instance['detail'] = str(change.description)
                elif change.status.name == 'Dikembalikan DGB':
                    app_history = ApplicationHistory.objects.get(change_record = change.id)
                    notes = Record.objects.filter(application_history = app_history.id)
                    log_instance['title'] = str(count) + ". " + str(change.status.name) + " pada " + str(change.created_at.date())
                    log_instance['detail'] = 'Catatan: '
                    log_instance['notes'] = notes
                else:
                    log_instance['title'] = str(count) + ". " + str(change.status.name) + " pada " + str(change.created_at.date())
                log += [log_instance]
                count += 1
            cyds_log += [log]
        return cyds_log
    except ObjectDoesNotExist:
        raise LogNotFound()

def cyd_diproses_list(diproses):
    try:
        status = get_status(diproses)
        jabatan_dituju = get_jabatan_dituju(diproses)
        val = list(diproses.values())
        i = 0
        while i < len(val):
            cyd = val[i]
            cyd['status'] = status[i]
            cyd['dest_level'] = jabatan_dituju[i]
            cyd['level'] = diproses[i].current_level.name
            val[i] = cyd
            i += 1
        return val
    except (StatusNotFound, JabatanNotFound):
        return []

def cyd_dikembalikan_list(dikembalikan):
    try:
        jabatan_dituju = get_jabatan_dituju(dikembalikan)
        log = get_log(dikembalikan)
        val = list(dikembalikan.values())
        i = 0
        while i < len(val):
            cyd = val[i]
            cyd['log'] = log[i]
            cyd['dest_level'] = jabatan_dituju[i]
            cyd['level'] = dikembalikan[i].current_level.name
            val[i] = cyd
            i += 1
        return val
    except (LogNotFound, JabatanNotFound):
        return []

def cyd_dibatalkan_list(dibatalkan):
    try:
        jabatan_dituju = get_jabatan_dituju(dibatalkan)
        log = get_log(dibatalkan)
        waktu = get_waktu(dibatalkan, 'Dibatalkan')
        val = list(dibatalkan.values())
        i = 0
        while i < len(val):
            cyd = val[i]
            cyd['log'] = log[i]
            cyd['dest_level'] = jabatan_dituju[i]
            cyd['level'] = dibatalkan[i].current_level.name
            cyd['time'] = waktu[i].date()
            val[i] = cyd
            i += 1
        return val
    except (LogNotFound, JabatanNotFound, WaktuNotFound):
        return []

def cyd_diterima_list(diterima):
    try:
        jabatan_dituju = get_jabatan_dituju(diterima)
        waktu = get_waktu(diterima, 'Diterima')
        val = list(diterima.values())
        i = 0
        while i < len(val):
            cyd = val[i]
            cyd['dest_level'] = jabatan_dituju[i]
            cyd['level'] = diterima[i].current_level.name
            cyd['time'] = waktu[i].date()
            val[i] = cyd
            i += 1
        return val
    except (JabatanNotFound, WaktuNotFound):
        return []