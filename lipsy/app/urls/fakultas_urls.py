"""
URL for fakultas
"""
from django.conf.urls import url
from django.contrib import admin
import app.views.fakultas_views as fv

urlpatterns = [
	url(r'^view/active/?', fv.fakultas_pantau_aktif, name='fakultas_pantau_aktif'),
	url(r'^view/canceled/?', fv.fakultas_pantau_dibatalkan, name='fakultas_pantau_dibatalkan'),
	url(r'^view/accepted/?', fv.fakultas_pantau_diterima, name='fakultas_pantau_diterima'),
	url(r'^view/download_berkas(?P<cyd_id>[0-9]+)', fv.download_berkas, name='download_berkas'),
	url(r'^view/download_acc(?P<cyd_id>[0-9]+)', fv.download_acc, name='download_acc'),
]
