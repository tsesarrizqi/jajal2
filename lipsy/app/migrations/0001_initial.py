# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-19 06:22
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Admin',
            fields=[
                ('nip', models.CharField(max_length=18, primary_key=True, serialize=False)),
                ('username', models.CharField(default='', max_length=30)),
                ('password', models.CharField(default='', max_length=30)),
                ('email', models.EmailField(default='', max_length=100)),
                ('name', models.CharField(default='', max_length=30)),
                ('token', models.CharField(max_length=100)),
                ('created_at', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_at', models.DateTimeField(blank=True, default=datetime.datetime.now)),
            ],
        ),
        migrations.CreateModel(
            name='Application',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('application_bundle_path', models.TextField()),
                ('acceptance_letter_file_path', models.TextField()),
                ('created_at', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_at', models.DateTimeField(blank=True, default=datetime.datetime.now)),
            ],
        ),
        migrations.CreateModel(
            name='ApplicationHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField()),
                ('revision_number', models.IntegerField()),
                ('created_at', models.DateTimeField(blank=True, default=datetime.datetime.now)),
            ],
        ),
        migrations.CreateModel(
            name='CalonYangDiusulkan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nip', models.CharField(max_length=18, unique=True)),
                ('name', models.CharField(max_length=50)),
                ('birthdate', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('birthplace', models.CharField(max_length=30)),
                ('email', models.CharField(max_length=50)),
                ('gender', models.CharField(max_length=1)),
                ('created_at', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_at', models.DateTimeField(blank=True, default=datetime.datetime.now)),
            ],
        ),
        migrations.CreateModel(
            name='EducationHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('order', models.IntegerField()),
                ('cyd', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.CalonYangDiusulkan')),
            ],
        ),
        migrations.CreateModel(
            name='Faculty',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Level',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Record',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('record', models.TextField()),
                ('created_at', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('application_history', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.ApplicationHistory')),
            ],
        ),
        migrations.CreateModel(
            name='Regulation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_at', models.DateTimeField(blank=True, default=datetime.datetime.now)),
            ],
        ),
        migrations.CreateModel(
            name='RegulationQuestion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.TextField()),
                ('created_at', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('updated_at', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('creator', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Admin')),
                ('regulation', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Regulation')),
            ],
        ),
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='StatusChangeRecord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField()),
                ('created_at', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('cyd', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.CalonYangDiusulkan')),
                ('status', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Status')),
            ],
        ),
        migrations.AddField(
            model_name='record',
            name='regulation_question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.RegulationQuestion'),
        ),
        migrations.AddField(
            model_name='calonyangdiusulkan',
            name='current_level',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Level'),
        ),
        migrations.AddField(
            model_name='calonyangdiusulkan',
            name='faculty',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Faculty'),
        ),
        migrations.AddField(
            model_name='applicationhistory',
            name='change_record',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='app.StatusChangeRecord'),
        ),
        migrations.AddField(
            model_name='applicationhistory',
            name='cyd',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.CalonYangDiusulkan'),
        ),
        migrations.AddField(
            model_name='application',
            name='cyd',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.CalonYangDiusulkan'),
        ),
        migrations.AddField(
            model_name='application',
            name='destination_level',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Level'),
        ),
        migrations.AddField(
            model_name='application',
            name='regulation',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Regulation'),
        ),
        migrations.AddField(
            model_name='application',
            name='status',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Status'),
        ),
        migrations.AddField(
            model_name='admin',
            name='role',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Role'),
        ),
    ]
