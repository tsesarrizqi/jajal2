from django.shortcuts import render
from app.controllers import fakultas_memantau_aplikasi_cyd as fmac

def fakultas_pantau_aktif(request):
    fakultas = 1
    dikembalikan = fmac.get_cyd(fakultas, 'Dikembalikan')
    dikembalikan_list = fmac.cyd_dikembalikan_list(dikembalikan)
    diproses = fmac.get_cyd(fakultas, 'Diproses')
    diproses_list = fmac.cyd_diproses_list(diproses)
    return render(request, 'app/fakultas/fakultas_pantau_aktif.html', {'cyd_dikembalikan':dikembalikan_list, 'cyd_diproses':diproses_list, "user" : { "role" : "Fakultas" }})

def fakultas_pantau_dibatalkan(request):
    fakultas = 1
    dibatalkan = fmac.get_cyd(fakultas, 'Dibatalkan')
    dibatalkan_list = fmac.cyd_dibatalkan_list(dibatalkan)
    return render(request, 'app/fakultas/fakultas_pantau_dibatalkan.html', {'cyd_dibatalkan':dibatalkan_list, "user" : { "role" : "Fakultas" }})

def fakultas_pantau_diterima(request):
    fakultas = 1
    diterima = fmac.get_cyd(fakultas, 'Diterima')
    diterima_list = fmac.cyd_diterima_list(diterima)
    return render(request, 'app/fakultas/fakultas_pantau_diterima.html', {'cyd_diterima':diterima_list, "user" : { "role" : "Fakultas" }})

def download_berkas(request, cyd_id):
    resp = fmac.download_berkas(cyd_id)
    return resp

def download_acc(request, cyd_id):
    resp = fmac.download_acc(cyd_id)
    return resp
